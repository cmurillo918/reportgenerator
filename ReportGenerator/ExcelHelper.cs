﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ReportGenerator
{
    public static class ExcelHelper
    {

        public static void Generate(DataTable dt, Report report)
        {
            const int headerRowIndex = 0;
            const int bodyStartIndex = 1;

            var workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet();

            FillHeader(dt, sheet, headerRowIndex);

            FillBody(dt, sheet, bodyStartIndex);

            Path.GetFileNameWithoutExtension(report.Filename);
            string path = string.Format("{0}\\{1}", report.Directory, report.Filename).AppendTimeStamp();


            using (System.IO.FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                workbook.Write(stream);
            }
        }

        private static void FillHeader(DataTable dt, ISheet sheet, int index)
        {
            var headerRow = sheet.CreateRow(index);

            int currentColumn = 0;

            foreach (var col in dt.Columns)
            {
                var cell = headerRow.CreateCell(currentColumn++);
                cell.SetCellValue(col.ToString());
            }
        }

        private static void FillBody(DataTable dt, ISheet sheet, int index)
        {
            var workbook = sheet.Workbook as XSSFWorkbook;

            int col = dt.Columns.Count;
            foreach (DataRow row in dt.Rows)
            {
                var excelRow = sheet.CreateRow(index);
                for (int i = 0; i < col; i++)
                {
                    var cell = excelRow.CreateCell(i);
                    SetCellValue(cell, row[i]);
                }
                index++;
            }

        }

        private static void SetCellValue(ICell cell, object value)
        {
            switch (Type.GetTypeCode(value.GetType()))
            {
                case TypeCode.DateTime:
                    cell.SetCellValue(Convert.ToDateTime(value).ToString("MM/dd/yyyy"));
                    break;
                default:
                    cell.SetCellValue(value.ToString());
                    break;
            }

        }

        private static string AppendTimeStamp(this string path)
        {
            return string.Concat(
                Path.GetDirectoryName(path),
                "\\",
                Path.GetFileNameWithoutExtension(path),
                DateTime.Now.ToString("_yyyy-MM-dd--HHmmss"),
                Path.GetExtension(path)
            );
        }

    }
}
