﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.DAL
{
    public static class DataReaderExtensions
    {

        public static bool ColumnExists(this SqlDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        public static int AsInt(this SqlDataReader dataReader, string column, int defaultValue = 0)
        {
            var value = dataReader[column];


            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return int.Parse(value.ToString());
        }

        public static double AsDouble(this SqlDataReader dataReader, string column, double defaultValue = 0)
        {
            var value = dataReader[column];


            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return double.Parse(value.ToString());
        }
        public static decimal AsDecimal(this SqlDataReader dataReader, string column, decimal defaultValue = 0)
        {
            var value = dataReader[column];


            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return decimal.Parse(value.ToString());
        }
        public static string AsString(this SqlDataReader dataReader, string column, string defaultValue = "")
        {

            var value = dataReader[column];

            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return value.ToString().Trim();
        }

        public static bool AsBoolean(this SqlDataReader dataReader, string column, bool defaultValue = false)
        {

            var value = dataReader[column];

            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return bool.Parse(value.ToString());
        }

        public static DateTime? AsDateTime(this SqlDataReader dataReader, string column)
        {

            var value = dataReader[column];

            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return null;

            return DateTime.Parse(value.ToString());
        }

        public static int AsInt(this DbDataReader dataReader, string column, int defaultValue = 0)
        {
            var value = dataReader[column];


            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return int.Parse(value.ToString());
        }

        public static int? AsIntNullable(this DbDataReader dataReader, string column, int? defaultValue = null)
        {
            var value = dataReader[column];


            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return int.Parse(value.ToString());
        }

        public static double AsDouble(this DbDataReader dataReader, string column, double defaultValue = 0)
        {
            var value = dataReader[column];


            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return double.Parse(value.ToString());
        }

        public static double? AsDoubleNullable(this DbDataReader dataReader, string column, double? defaultValue = null)
        {
            var value = dataReader[column];


            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return double.Parse(value.ToString());
        }
        public static decimal AsDecimal(this DbDataReader dataReader, string column, decimal defaultValue = 0)
        {
            var value = dataReader[column];


            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return decimal.Parse(value.ToString());
        }
        public static string AsString(this DbDataReader dataReader, string column, string defaultValue = "")
        {

            var value = dataReader[column];

            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return value.ToString().Trim();
        }

        public static bool AsBoolean(this DbDataReader dataReader, string column, bool defaultValue = false)
        {

            var value = dataReader[column];

            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return defaultValue;

            return bool.Parse(value.ToString());
        }

        public static DateTime? AsDateTime(this DbDataReader dataReader, string column)
        {

            var value = dataReader[column];

            if (value == null || value == DBNull.Value || string.IsNullOrWhiteSpace(value.ToString()))
                return null;

            return DateTime.Parse(value.ToString());
        }
    }
}
