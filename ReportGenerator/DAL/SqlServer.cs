﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;

namespace ReportGenerator.DAL
{
    public static class SqlServer
    {
        private static string ConnectionString = ConfigurationManager.AppSettings["connectionString"];
        private static DbProviderFactory Factory
        {
            get
            {
                return DbProviderFactories.GetFactory("System.Data.SqlClient");
            }
        }

        public static object NullIfEmpty(object value)
        {
            if (value == null) { return DBNull.Value; }
            if (string.IsNullOrWhiteSpace(value.ToString())) { return DBNull.Value; }

            return value;
        }

        public static DbDataReader GetDataReader(string sql, SqlParameterCollection parameters = null, CommandType commandType = CommandType.Text)
        {
            var connection = Factory.CreateConnection();

            var command = Factory.CreateCommand();

            connection.ConnectionString = ConnectionString;
            command.Connection = connection;
            command.CommandType = commandType;
            command.CommandText = sql;

            connection.Open();

            if (parameters != null)
            {
                foreach (SqlParameter thisParam in parameters)
                {
                    command.Parameters.Add(new SqlParameter(thisParam.ParameterName, thisParam.Value));
                }
            }

            return command.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public static DataTable GetTable(string sql, SqlParameterCollection parameters, CommandType commandType = CommandType.Text)
        {
            using (var connection = Factory.CreateConnection())
            {
                using (var command = Factory.CreateCommand())
                {
                    connection.ConnectionString = ConnectionString;
                    command.Connection = connection;
                    command.CommandType = commandType;
                    command.CommandText = sql;
                    command.CommandTimeout = 90;
                    connection.Open();

                    if (parameters != null)
                    {
                        foreach (SqlParameter thisParam in parameters)
                        {
                            command.Parameters.Add(new SqlParameter(thisParam.ParameterName, thisParam.Value));
                        }
                    }

                    using (var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {

                        using (var table = new DataTable())
                        {
                            table.Load(dataReader);

                            return table;
                        }
                    }
                }
            }
        }

        public static void ExecuteNonQuery(string sql, SqlParameterCollection parameters, CommandType commandType = CommandType.Text)
        {
            using (var connection = Factory.CreateConnection())
            {
                using (var command = Factory.CreateCommand())
                {
                    connection.ConnectionString = ConnectionString;
                    command.Connection = connection;
                    command.CommandType = commandType;
                    command.CommandText = sql;
                    connection.Open();
                    if (parameters != null)
                    {
                        foreach (SqlParameter thisParam in parameters)
                        {
                            command.Parameters.Add(new SqlParameter(thisParam.ParameterName, thisParam.Value));
                        }
                    }

                    command.ExecuteNonQuery();
                }
            }
        }

        public static SqlConnection GetConnection()
        {
            var connection = new SqlConnection();
            connection.ConnectionString = ConnectionString;
            return connection;
        }

        public static object ExecuteScalar(string sql, SqlParameterCollection parameters, CommandType commandType = CommandType.Text)
        {
            using (var connection = Factory.CreateConnection())
            {
                using (var command = Factory.CreateCommand())
                {
                    connection.ConnectionString = ConnectionString;
                    command.Connection = connection;
                    command.CommandType = commandType;
                    command.CommandText = sql;

                    connection.Open();

                    if (parameters != null)
                    {
                        foreach (SqlParameter thisParam in parameters)
                        {
                            command.Parameters.Add(new SqlParameter(thisParam.ParameterName, thisParam.Value));
                        }
                    }

                    return command.ExecuteScalar();
                }
            }
        }
    }
}
