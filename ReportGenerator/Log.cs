﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator
{
    public static class Log
    {
        private static string _logDirectory = System.Configuration.ConfigurationManager.AppSettings["LogDirectory"];
        private static StringBuilder _log = new System.Text.StringBuilder();

        private static int fileNotFound = 0;
        private static int fileEmpty = 0;

        private static void AppendExceptionSummary()
        {
            Append(string.Format("Files not found: {0}", fileNotFound));
            Append(string.Format("Empty files: {0}", fileEmpty));
        }

        public static void Append(string msg)
        {
            Console.WriteLine(msg);
            _log.AppendLine(msg);
        }

        public static void LogException(Exception e)
        {
            Append("---------------------------------------------------------------------------------------------------------------------");
            Append("AN UNCAUGHT EXCEPTION OCCURRED:");
            Append(e.Message);
            Append("---------------------------------------------------------------------------------------------------------------------");
            Append(e.StackTrace);
            Append("---------------------------------------------------------------------------------------------------------------------");
            Append(e.Source.ToString());
            Append("---------------------------------------------------------------------------------------------------------------------");

            GenerateLogFile();
            Environment.Exit(0);
        }

        public static void LogFileNotFound(string msg)
        {
            fileNotFound++;
            Append(msg);
        }

        public static void LogFileEmpty(string msg)
        {
            fileEmpty++;
            Append(msg);
        }

        public static void GenerateLogFile()
        {
            AppendExceptionSummary();
            string fileName = string.Format("CADHandleImport Log-{0:yyyy-MM-dd_hh-mm-ss-tt}.txt", DateTime.Now);
            string path = Path.Combine(_logDirectory, fileName);
            if (!Directory.Exists(_logDirectory))
            {
                Directory.CreateDirectory(_logDirectory);
            }
            File.WriteAllText(path, _log.ToString());
        }
    }
}
