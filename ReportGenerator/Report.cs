﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator
{
    public class Report
    {
        public int ReportId { get; set; }
        public string ReportName { get; set; }
        public string Title { get; set; }
        public string Filename { get; set; }
        public string StoredProcedure { get; set; }
        public string Directory { get; set; }
        public string Frequency { get; set; }
        public DateTime? LastRunDate { get; set; }
        public DateTime? StartDate { get; set; }
        public bool IsActive { get; set; }

        public Enums.Day Day { get; set; }
        public bool Run
        {
            get
            {
                if (!IsActive || StartDate > DateTime.Now) return false;
                if (Day != Enums.Day.None && Day != (Enums.Day)DateTime.Now.DayOfWeek) return false;
                if (LastRunDate == null) return true;
                switch (Frequency.ToLower())
                {
                    case "d":
                        return LastRunDate.Value.Date <= DateTime.Now.AddDays(-1);
                    case "w":
                        return LastRunDate.Value.Date <= DateTime.Now.AddDays(-7);
                    case "m":
                        return LastRunDate.Value.Date <= DateTime.Now.AddMonths(-1);
                    default:
                        return false;
                }
            }
        }
    }
}
