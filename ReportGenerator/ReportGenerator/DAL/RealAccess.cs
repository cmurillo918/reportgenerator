﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ReportGenerator.DAL
{
    public static class RealAccess
    {
        public static IList<Report> GetReports()
        {
            List<Report> reports = new List<Report>();
            string sql = @"SELECT * FROM tblReport WHERE IsActive = 1 AND StartDate <= GETDATE()";
            using (var reader = SqlServer.GetDataReader(sql, null, CommandType.Text))
            {
                while (reader.Read())
                {
                    Report r = new Report();
                    r.Filename = reader.AsString("Filename");
                    r.StoredProcedure = reader.AsString("StoredProcedure");
                    r.Directory = reader.AsString("Directory");
                    r.Frequency = reader.AsString("Frequency");
                    r.LastRunDate = reader.AsDateTime("LastRunDate");
                    reports.Add(r);
                }
            }
            return reports;
        }

        public static DataTable GetReportData(Report r)
        {
            return SqlServer.GetTable(r.StoredProcedure, null, CommandType.StoredProcedure);
        }

        public static void UpdateReportLastRunDate(int reportId)
        {
            using(SqlCommand cmd = new SqlCommand())
            {
                string sql = @"UPDATE tblReport SET LastRunDate = GETDATE() WHERE ReportId = @ReportId";
                cmd.Parameters.AddWithValue("@ReportId", reportId); 
                SqlServer.ExecuteNonQuery(sql, cmd.Parameters, CommandType.Text);
            }
            
        }

    }
}
