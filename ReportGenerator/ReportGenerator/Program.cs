﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ReportGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Report> reports = DAL.RealAccess.GetReports().ToList();
           

            foreach (Report r in reports.Where(r => r.Run))
            {
                DataTable dt = DAL.RealAccess.GetReportData(r);
                ExcelHelper.Generate(dt, r);
                DAL.RealAccess.UpdateReportLastRunDate(r.ReportId);
            }
        }

        static void GetReportsToRun(ref List<Report> reports)
        {
            List<Report> filtered = new List<Report>();
            foreach (Report r in reports)
            {
                if (r.LastRunDate == null)
                {
                    filtered.Add(r);
                    continue;
                };
                bool run = false;
                switch (r.Frequency.ToLower())
                {
                    case "d":
                        run = r.LastRunDate.Value.Date <= DateTime.Now.AddDays(-1);
                        break;
                    case "w":
                        run = r.LastRunDate.Value.Date <= DateTime.Now.AddDays(-7);
                        break;
                    case "m":
                        run = r.LastRunDate.Value.Date <= DateTime.Now.AddMonths(-1);
                        break;
                }
                if (run)
                    filtered.Add(r);
            }
            reports = filtered;

        }
    }
}
