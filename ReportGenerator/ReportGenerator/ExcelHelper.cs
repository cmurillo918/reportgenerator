﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ReportGenerator
{
    public static class ExcelHelper
    {

        public static XSSFWorkbook Generate(DataTable dt, Report report)
        {
            const int headerRowIndex = 0;
            const int bodyStartIndex = 1;

            var workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet();

            FillHeader(dt, sheet, headerRowIndex);

            FillBody(dt, sheet, bodyStartIndex);

            Path.GetFileNameWithoutExtension(report.Filename);
            string ext = Path.GetExtension(report.Filename);
            string path = string.Format("{0}\\{1}", report.Directory, report.Filename);
            

            using(System.IO.FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                workbook.Write(stream);
            }


            return workbook;


        }

        public static void FillHeader(DataTable dt, ISheet sheet, int index)
        {
            var headerCellStyle = GetHeaderCellStyle(sheet.Workbook as XSSFWorkbook);
            var headerRow = sheet.CreateRow(index);

            int currentColumn = 0;

            foreach (var col in dt.Columns)
            {
                var cell = headerRow.CreateCell(currentColumn++);
                //cell.CellStyle = headerCellStyle;
                cell.SetCellValue(col.ToString());
            }
        }

        public static void FillBody(DataTable dt, ISheet sheet, int index)
        {
            int col = dt.Columns.Count;
            foreach (DataRow row in dt.Rows)
            {
                var excelRow = sheet.CreateRow(index);  
                for (int i=0; i< col; i++)
                {
                    var cell = excelRow.CreateCell(i);
                    cell.SetCellValue(row[i].ToString());
                }
                index++;
            }

        }

        private static ICellStyle GetHeaderCellStyle(XSSFWorkbook workbook)
        {
            var headerCellStyle = workbook.CreateCellStyle();
            var headerFont = workbook.CreateFont();


            headerFont.Boldweight = (short)FontBoldWeight.Bold;
            headerCellStyle.SetFont(headerFont);
            headerCellStyle.WrapText = true;
            return headerCellStyle;
        }



    }
}
