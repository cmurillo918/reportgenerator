﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ReportGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                List<Report> reports = DAL.RealAccess.GetReports().ToList();
                List<Report> reportsToRun = reports.Where(r => r.Run).ToList();

                foreach (Report r in reportsToRun)
                {
                    DataTable dt = DAL.RealAccess.GetReportData(r);
                    ExcelHelper.Generate(dt, r);

                    DAL.RealAccess.UpdateReportLastRunDate(r.ReportId);
                }

            }
            catch(Exception e)
            {
                Log.LogException(e);
            }
            
        }
    }
}
